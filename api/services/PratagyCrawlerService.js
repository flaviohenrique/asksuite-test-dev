class PratagyCrawlerService {

    static getUrl(checkin, checkout) {
        const checkindate = new Date(checkin + " 00:00:00")
        const checkoutdate = new Date(checkout + " 00:00:00")
        return `https://pratagy.letsbook.com.br/D/Reserva?checkin=${checkindate.getDate()}%2F${checkindate.getMonth() + 1}%2F${checkindate.getFullYear()}&checkout=${checkoutdate.getDate()}%2F${checkoutdate.getMonth() + 1}%2F${checkoutdate.getFullYear()}&cidade=&hotel=12&adultos=2&criancas=&destino=Pratagy+Beach+Resort+All+Inclusive&promocode=&tarifa=&mesCalendario=6%2F14%2F2022`
    }

    static async getListRooms(page) {
        page.waitFor(1000)
        
        const roomList = await page.evaluate(() => {
            if (document.querySelector("div.hotel-selecionado-indisponivel-titulo") != null ) {
                return document.querySelector("div.hotel-selecionado-indisponivel-titulo").textContent
            }
            const roomList = [];
            const rooms =  document.querySelectorAll("tr.row-quarto");
            for (const room of rooms) {
                roomList.push({
                    name: room.querySelector('span.quartoNome').innerText,
                    description: room.querySelector('div.quartoDescricao > p').innerText,
                    price: room.querySelector('span.valorFinal').innerText,
                    image: room.querySelector('img.room--image').getAttribute('data-src')
                });
            }
            return roomList;
        })
        return roomList
    }
}

module.exports = PratagyCrawlerService;