const BrowserService = require("../services/BrowserService")
const PratagyCrawlerService = require("../services/PratagyCrawlerService")

module.exports.index = function (application, req, res) {

  BrowserService.getBrowser().then(async (puppeteer) => {

    const page = await puppeteer.newPage()
    const url = await PratagyCrawlerService.getUrl(req.body.checkin,req.body.checkout)

    await page.goto(url, { waitUntil: 'load' })
    PratagyCrawlerService.getListRooms(page).then((roomList)=>{
      BrowserService.closeBrowser(page)
      res.status(200).json(roomList);
    })
  })
}