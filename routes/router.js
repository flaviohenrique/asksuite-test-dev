const express = require('express');
const BrowserService = require('../api/services/BrowserService');
const router = express.Router();

const searchController = require('../api/controllers/searchController');
const { application } = require('express');

router.get('/', (req, res) => {
    return res.status(200).json({ message: 'Hello Asksuite World!' });
});

router.post("/search", function(req, res){
    searchController.index(application, req, res)
})

module.exports = router;